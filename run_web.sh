#!/bin/sh

# wait for PSQL server to start
sleep 10

cd media_app
# prepare init migration
su -m testuser -c "python manage.py makemigrations media_app --settings=settings.base"
# migrate db, so we have the latest db schema
su -m testuser -c "python manage.py migrate --settings=settings.base"
# start development server on public ip interface, on port 8000
su -m testuser -c "python manage.py runserver 0.0.0.0:8000 --settings=settings.base"