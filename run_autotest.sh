#!/bin/sh

# wait for PSQL server to start
sleep 10

cd media_app

su -m testuser -c "python manage.py test --settings=settings.test"