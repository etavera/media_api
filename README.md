### Stack
* Python 3.6
* Django 2.1
* Postgresql
* Django Rest Framework
* Celery
* Redis
* Docker
* docker-compose

#### Run dockers.
From root folder of repository run commands:

<i>docker-compose build</i> <br>
<i>docker-compose up runserver</i>

#### Run autotests:
<i>docker-compose up autotest</i>

### Api example:

#### List of pages

http://0.0.0.0:8000/pages/ <br>
http://0.0.0.0:8000/pages/?page=2

#### Page details:

http://0.0.0.0:8000/pages/1/ <br>
http://0.0.0.0:8000/pages/2/ <br>
http://0.0.0.0:8000/pages/4/

#### Admin
http://0.0.0.0:8000/admin/

login: <i>admin</i> <br>
password: <i>admin</i>

