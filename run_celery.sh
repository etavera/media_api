#!/bin/sh

cd media_app
# run Celery worker for our project myproject with Celery configuration stored in Celeryconf

su -m testuser -c "celery worker -A media_app.celeryconf -Q default -n default@%h"