# -*- coding:utf-8 -*-

from rest_framework import serializers

from .models import Page, Video, Audio, Text


class PageSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Page
        fields = ['title', 'url']


class VideoSerializer(serializers.ModelSerializer):

    media_type = serializers.CharField(default=Video.MEDIA_TYPE)
    path = serializers.CharField(source='video.path')
    subtitle = serializers.CharField(source='video.subtitle')

    class Meta:
        model = Video
        fields = ['id', 'title', 'path', 'subtitle', 'media_type', 'counter']


class AudioSeraliazer(serializers.ModelSerializer):

    media_type = serializers.CharField(default=Audio.MEDIA_TYPE)
    bitrate = serializers.IntegerField(source='audio.bitrate')

    class Meta:
        model = Audio
        fields = ['id', 'title', 'bitrate', 'media_type', 'counter']


class TextSerializer(serializers.ModelSerializer):

    media_type = serializers.CharField(default=Audio.MEDIA_TYPE)
    text = serializers.CharField(source='text.text_field')

    class Meta:
        model = Text
        fields = ['id', 'title', 'text', 'media_type', 'counter']
