# -*- coding:utf-8 -*-

from django.shortcuts import get_object_or_404

from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response

from .serializers import PageSerializer, AudioSeraliazer, VideoSerializer, TextSerializer
from .models import Page, Audio, Video, Text
from .tasks import inc_counter_task


class PageList(generics.ListCreateAPIView):
    """
    API endpoint that represents a list of pages.
    """
    model = Page
    serializer_class = PageSerializer
    queryset = Page.objects.all().order_by('pk')


class PageDetail(APIView):
    '''
    API endpoint that reprents a page details:
        - audio
        - video
        - etc
    '''

    MEDIA_SERIALIZERS_MAP = {
        Video.MEDIA_TYPE: VideoSerializer,
        Audio.MEDIA_TYPE: AudioSeraliazer,
        Text.MEDIA_TYPE: TextSerializer
    }

    def get(self, request, pk, format=None):

        '''
        Api endpoints that represents page with media list belonging to current page.
        '''
        page = get_object_or_404(Page, pk=pk)
        res = {'page_id': page.id, 'page_title': page.title, 'media': []}

        for pm in page.page_media_set.all().order_by('order'):
            # get media object: Audio, Video, etc
            serializer = self.MEDIA_SERIALIZERS_MAP.get(pm.media.media_type)
            l = serializer(pm.media).data
            l['order'] = pm.order
            res['media'].append(l)
            # increment counter for media in the background
            inc_counter_task.delay(media_id=pm.media.id)

        return Response(res)


# end