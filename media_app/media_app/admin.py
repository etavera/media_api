# -*- coding:utf-8 -*-

from django.contrib import admin

from .models import Page, Video, Audio, Page_Media, Text


class PageMediaInline(admin.TabularInline):
    model = Page_Media
    extra = 1
    verbose_name = 'Media'


class MediaAdminBase(admin.ModelAdmin):

    search_fields = ['title', ]

    # save media_type values
    def save_model(self, request, obj, form, change):
        obj.media_type = self.verbose_name
        obj.save()


class VideoAdmin(MediaAdminBase):

    list_display = ['title', 'path', 'subtitle']
    fields = ['title', 'path', 'subtitle']
    verbose_name = Video.MEDIA_TYPE


class AudioAdmin(MediaAdminBase):

    list_display = ['title', 'bitrate']
    fields = ['title', 'bitrate']
    verbose_name = Audio.MEDIA_TYPE


class TextAdmin(MediaAdminBase):

    list_display = ['title', 'text_field']
    fields = ['title', 'text_field']
    verbose_name = Text.MEDIA_TYPE


class PageAdmin(admin.ModelAdmin):

    list_display = ['title']
    search_fields = ['title', ]
    inlines = [PageMediaInline, ]
    ordering = ['pk']


admin.site.register(Video, VideoAdmin)
admin.site.register(Audio, AudioAdmin)
admin.site.register(Text, TextAdmin)
admin.site.register(Page, PageAdmin)