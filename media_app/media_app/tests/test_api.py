# -*- coding:utf-8 -*-

from django.urls import reverse

from rest_framework import status
from rest_framework.test import APITestCase


class PageListApiTests(APITestCase):

    maxDiff = None

    def test_get_pages(self):

        """
        get first page of page list
        """
        url = reverse('page-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual({
                            "count": 7,
                            "next": "http://testserver/pages/?page=2",
                            "previous": None,
                            "results": [
                                {
                                    "title": "page1",
                                    "url": "http://testserver/pages/1/"
                                },
                                {
                                    "title": "page2",
                                    "url": "http://testserver/pages/2/"
                                },
                                {
                                    "title": "page3",
                                    "url": "http://testserver/pages/3/"
                                },
                                {
                                    "title": "page4",
                                    "url": "http://testserver/pages/4/"
                                },
                                {
                                    "title": "page5",
                                    "url": "http://testserver/pages/5/"
                                }
                            ]},
                                response.json())

    def test_get_second_page(self):
        url = reverse('page-list')
        response = self.client.get(url, data={'page':2})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual({
                            "count": 7,
                            "next": None,
                            "previous": "http://testserver/pages/",
                            "results": [
                                {
                                    "title": "page6",
                                    "url": "http://testserver/pages/6/"
                                },
                                {
                                    "title": "page7",
                                    "url": "http://testserver/pages/7/"
                                }
                                ]
                            },
                             response.json())

    def test_404(self):
        url = reverse('page-list')
        response = self.client.get(url, data={'page': 10})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_404_str(self):
        url = reverse('page-list')
        response = self.client.get(url, data={'page': 'sss'})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class PageApiTest(APITestCase):

    maxDiff = None

    def test_get_page_1(self):
        url = reverse('page-detail', args=[1, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        media_json = {
                "page_id": 1,
                "page_title": "page1",
                "media": [
                    {
                        "id": 10,
                        "title": "Video 5",
                        "path": "path 5",
                        "subtitle": "subtitle 5",
                        "media_type": "video",
                        "counter": 0,
                        "order": 1
                    },
                    {
                        "id": 3,
                        "title": "title3",
                        "path": "path3",
                        "subtitle": "subtitle3",
                        "media_type": "video",
                        "counter": 0,
                        "order": 2
                    },
                    {
                        "id": 5,
                        "title": "audio2",
                        "bitrate": 64,
                        "media_type": "audio",
                        "counter": 0,
                        "order": 3
                    },
                    {
                        "id": 9,
                        "title": "Video title 4",
                        "path": "path 4",
                        "subtitle": "subtitle 4",
                        "media_type": "video",
                        "counter": 0,
                        "order": 4
                    }
                ]
            }
        self.assertDictEqual( media_json, response.json() )

    def test_get_page_4(self):
        url = reverse('page-detail', args=[4, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        media_json = {
                    "page_id": 4,
                    "page_title": "page4",
                    "media": [
                        {
                            "id": 4,
                            "title": "audio1",
                            "bitrate": 192,
                            "media_type": "audio",
                            "counter": 0,
                            "order": 1
                        },
                        {
                            "id": 2,
                            "title": "Video title 2",
                            "path": "path2",
                            "subtitle": "subtitle2",
                            "media_type": "video",
                            "counter": 0,
                            "order": 2
                        },
                        {
                            "id": 9,
                            "title": "Video title 4",
                            "path": "path 4",
                            "subtitle": "subtitle 4",
                            "media_type": "video",
                            "counter": 0,
                            "order": 3
                        },
                        {
                            "id": 7,
                            "title": "Audio title 4",
                            "bitrate": 256,
                            "media_type": "audio",
                            "counter": 0,
                            "order": 4
                        }
                    ]
                }
        self.assertDictEqual( media_json, response.json())


    def test_get_page_2(self):
        url = reverse('page-detail', args=[2, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        media_json = {
                "page_id": 2,
                "page_title": "page2",
                "media": [
                    {
                        "id": 13,
                        "title": "Морж",
                        "text": "три мужика делили шкуру\r\nот неубитого моржа\r\nдоход расход налоги прибыль\r\nмаржа",
                        "media_type": "text",
                        "counter": 0,
                        "order": 1
                    },
                    {
                        "id": 6,
                        "title": "Audio title 3",
                        "bitrate": 320,
                        "media_type": "audio",
                        "counter": 0,
                        "order": 2
                    },
                    {
                        "id": 2,
                        "title": "Video title 2",
                        "path": "path2",
                        "subtitle": "subtitle2",
                        "media_type": "video",
                        "counter": 0,
                        "order": 3

                    },
                    {
                        "id": 11,
                        "title": "Text title1",
                        "text": "как так открыть мне холодильник \r\nчтобы не видеть колбасу",
                        "media_type": "text",
                        "counter": 0,
                        "order": 4
                    },
                    {
                        "id": 12,
                        "title": "Text title 2",
                        "text": "к коту вдруг ёлка потянула \r\nсто сорок восемь хищных лап с \r\nтех пор он знает что такое \r\nколлапс",
                        "media_type": "text",
                        "counter": 0,
                        "order": 5
                    }
                ]
            }
        self.assertDictEqual(media_json, response.json())


    def test_empty_page(self):
        url = reverse('page-detail', args=[5, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual({
                                "page_id": 5,
                                "page_title": "page5",
                                "media": []
                            },
                            response.json())

    def test_404(self):
        url = reverse('page-detail', args=[1000, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)