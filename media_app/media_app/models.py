# -*- coding:utf-8 -*-

from django.db import models


class Page(models.Model):

    ctime = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=200, db_index=True)


class MediaBase(models.Model):
    '''
    Base abstract class for media models.
    '''

    MEDIA_CHOICES = (('video', 'video'),
                     ('audio', 'audio'),
                     ('text', 'text'))

    # common fields
    title = models.CharField(max_length=100, db_index=True)
    ctime = models.DateTimeField(auto_now_add=True)
    counter = models.IntegerField(default=0)
    media_type = models.CharField(max_length=5, choices=MEDIA_CHOICES, null=True)

    page = models.ManyToManyField(Page, through='Page_Media',
                                  through_fields=['media', 'page'])

    def inc_counter(self):
        '''
        increment media counter
        :return:
        '''
        self.counter = self.counter + 1
        self.save()

    def __str__(self):
        return '{} ({})'.format(self.title, self.media_type)

    class Meta:
        verbose_name = 'media'


class Video(MediaBase):

    MEDIA_TYPE = 'video'

    path = models.CharField(max_length=200, default='')
    subtitle = models.CharField(max_length=200, default='')


class Audio(MediaBase):

    MEDIA_TYPE = 'audio'

    bitrate = models.PositiveIntegerField(default=0)


class Text(MediaBase):

    MEDIA_TYPE = 'text'

    text_field = models.TextField(default='')


class Page_Media(models.Model):

    '''
    Intemediate model between pages and diffrent types of media
    '''
    page = models.ForeignKey(Page, db_index=True, on_delete=models.CASCADE)
    media = models.ForeignKey(MediaBase, db_index=True, on_delete=models.CASCADE)
    order = models.PositiveIntegerField(default=0)
    ctime = models.DateTimeField(auto_now_add=True)





