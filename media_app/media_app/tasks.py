# -*- coding:utf-8 -*-

from media_app.celeryconf import app
from .models import MediaBase


@app.task
def inc_counter_task(media_id):
    # Increment Audio/Video counter
    try:
        media_obj = MediaBase.objects.get(id=media_id)
        media_obj.inc_counter()
    except MediaBase.DoesNotExist:
        pass
